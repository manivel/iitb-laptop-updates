#!/bin/bash

# Version 0.1
# Srikant Patnaik (srikant@fossee.in)
# Manivel Rajendran
# Date 9 Oct 2018

######################
function check_kernel {
    sudo -K
    kver=$(uname -r)
    if [ "$kver" == "4.14.13-iitb-amd64" ]; then
        true
    else
        echo 
        echo "This update is incompatible with your operating system. Contact ld@iitb.ac.in for more details."
        echo "Exiting."
        exit 0
    fi
}

######################
function check_sudo {
    SUDO=''
    if [ $EUID != 0 ]; then
        SUDO='sudo'
    fi
}

######################
function set_gpio {
    $SUDO echo "@reboot root echo 449 > /sys/class/gpio/export && echo 1 > /sys/class/gpio/gpio449/value" > /tmp/speaker
    $SUDO mv /tmp/speaker /etc/cron.d/speaker 
    $SUDO chown root.root /etc/cron.d/speaker 
    if [ $? -eq 0 ]; then
        sleep 1
        echo
        echo "Successfully applied the update."
        echo "Reboot your system and try audio through speakers."
    else
        echo "Some thing went wrong. Please Re-run this program"
    fi
}
######################

check_kernel
check_sudo
set_gpio
