README
======

This update addresses the following 
-----------------------------------

1. Add support to audio jack detection (Automatically switch between a headphone jack and speakers)
#. Add support to microphone
#. Provides a clear audio through headphones and speakers
#. Improves Bluetooth stability


Known issues
------------

1. The wireless signal strength on the system tray appear low, however, the reception and connectivity remain good.
   This issue will not affect your WiFi performance. We will fix this minor issue with a patch soon.

Build
-----

The update-2.run file is a shell-archive, it is generated using a ``makeself`` utility:: 
    
    makeself --noprogress --target /tmp/payload  payload update-2.run "update 2" /tmp/payload/update-2.sh
