#!/bin/bash

# This update attends multiple bugs through a Kernel update
# This also removes some of the conf files which are no longer required

# Changelog
# a) Add support to audio jack detection (Automatically switch between headphone jack and speakers)
# b) Add support to microphone (not in this release)
# c) Provides a clear audio through headphones and speakers
# d) Improves bluetooth stability
# e) Check and mounts internal hard drive with read/write permissions (not in this release)

# Known issues
# a) The wireless signal strength on the system tray appear low, however, the reception and connectivity remains good.
#    This issue will not affect your WiFi performance. We will fix minor issue with a patch soon.


# Signed off: Srikant Patnaik (srikant@fossee.in)

check_bios() {
	#Check BIOS date and version
	[ $(cat /sys/class/dmi/id/bios_date) != "08/28/2018" ] && echo "Not the IIT RDP Laptop, exiting." && exit 2
	[ $(cat /sys/class/dmi/id/bios_version) != "WH-BI-12.5-Y116CR600-230-A" ] && echo "Not the IIT RDP Laptop, exiting." && exit 2
}

remove_files() {
	dpkglock="/var/lib/dpkg/lock"
	[ -e $dpkglock ] && sudo rm -fv $dpkglock

	dpkglists="/var/lib/apt/lists"
	[ -d $dpkglists ] && sudo rm -rfv $dpkglists

	btrule="/etc/udev/rules.d/bluetooth_r8723bs.rules"
	[ -e $btrule ] && sudo rm -fv $btrule

	sndfile="/etc/cron.d/speaker"
	[ -e $sndfile ] && sudo rm -fv $sndfile

	btsystemd1="/etc/systemd/system/bluetooth.service.wants/r8723bs_bluetooth.service"
	btsystemd2="/etc/systemd/system/r8723bs_bluetooth.service"
	[ -e $btsystemd1 ] && sudo unlink $btsystemd1
	[ -e $btsystemd2 ] && sudo rm -fv $btsystemd2


	rtsystemd1="/etc/systemd/system/sound.target.wants/bytcr_rt5651_sound.service"
	rtsystemd2="/etc/systemd/system/bytcr_rt5651_sound.service"
	[ -e $rtsystemd1 ] && sudo unlink  $rtsystemd1
	[ -e $rtsystemd2 ] && sudo rm -fv $rtsystemd2

	rmrdp="/home/rdp"
	[ -d $rmrdp ] && sudo rm -rvf $rmrdp
	
	rmhardware="/root/hardware"
	[ -d $rmhardware ] && sudo rm -rvf $rmhardware

}
# Check for internal drive mount

# Update the kernel
apply_update() {
	sudo cp -v $PWD/bytcr-rt5651/asound.state /var/lib/alsa/asound.state 
	echo "@reboot root cp /usr/share/alsa/ucm/bytcr-rt5651/asound.state /var/lib/alsa/asound.state" > /tmp/payload/asound
    # See if pulseaudio -k is needed by the user?
	sudo cp -v /tmp/payload/asound /etc/cron.d/asound
	sudo chmod 644 /etc/cron.d/asound
	sudo rm -rvf /usr/share/alsa/ucm/bytcr-rt5651
	sudo cp -rv $PWD/bytcr-rt5651 /usr/share/alsa/ucm/
	sudo chmod 755 -R /usr/share/alsa/ucm/bytcr-rt5651
	sudo cp -v $PWD/bytcr-rt5651/asound.state /var/lib/alsa/asound.state 
	sudo cp -v $PWD/rtl8723bs_config-OBDA8723.bin /lib/firmware/rtl_bt/rtl8723bs_config-OBDA8723.bin
	sudo cp -v $PWD/rtl8723bs_fw.bin /lib/firmware/rtl_bt/rtl8723bs_fw.bin
   	echo "blacklist evbug" > /tmp/payload/evbug.conf
    	sudo cp -v /tmp/payload/evbug.conf /etc/modprobe.d/evbug.conf
	sudo chmod 644 /etc/modprobe.d/evbug.conf
	cat /etc/modprobe.d/evbug.conf
	wget https://ld.iitb.ac.in/debian/PUBKEY.txt -O /tmp/payload/PUBKEY.txt
	sudo apt-key add - < /tmp/payload/PUBKEY.txt
	sudo apt update
	sudo apt install -y linux-image-4.19.0-rc7-iitb
	sudo apt install -y wireless-regdb crda
	echo 'SUBSYSTEM=="ieee80211", ACTION=="add", RUN+="/lib/crda/setregdomain"' > /tmp/payload/40-crda.rules
    	sudo cp -v /tmp/payload/40-crda.rules /etc/udev/rules.d/40-crda.rules
	sudo chmod 644 /etc/udev/rules.d/40-crda.rules
	cat /etc/udev/rules.d/40-crda.rules
}

clean() {
    rm -rvf /tmp/payload
}

#################
main () {
	check_bios
	remove_files
	apply_update
	clean
} 

main
